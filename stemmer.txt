STEMMER:
1. Baixar-se i instal�lar Snowball (http://snowballstem.org/download.html)
2. Baixar-se l'Script d'Snowball en catala (http://snowballstem.org/algorithms/catalan/stemmer.html)
3. Crear els fitxers C: snowball stemmer.sbl -o stemmer (http://snowballstem.org/runtime/use.html)
4. Incorporar els fitxers a Pystemmer (https://github.com/snowballstem/pystemmer)
	- libstemmer_c/libstemmer/*  -> Afegir el catala
	- Copiar el .h i .c a libstemmer_c/src_c
	- Installar Cython (pip install Cython)
5. Instal�lat Stemmer:
	- python setup.py install
	- sudo python setup.py install

6. Importar llibreria: 
	- import Stemmer (Quickstart: https://github.com/snowballstem/pystemmer/blob/master/docs/quickstart.txt)